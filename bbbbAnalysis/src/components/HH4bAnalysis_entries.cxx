#include "../BaselineVarsBoostedAlg.h"
#include "../BaselineVarsResolvedAlg.h"
#include "../JetPairingAlg.h"
#include "../JetBoostHistogramsAlg.h"
#include "../MassPlaneBoostHistogramsAlg.h"

using namespace HH4B;

DECLARE_COMPONENT(BaselineVarsBoostedAlg)
DECLARE_COMPONENT(BaselineVarsResolvedAlg)
DECLARE_COMPONENT(JetPairingAlg)
DECLARE_COMPONENT(JetBoostHistogramsAlg)
DECLARE_COMPONENT(MassPlaneBoostHistogramsAlg)
