#include "../BTaggingDecoratorAlg.h"
#include "../EventCounterAlg.h"
#include "../GhostAssocVRJetGetterAlg.h"
#include "../JetSelectorAlg.h"
#include "../PhotonSelectorAlg.h"
#include "../MuonSelectorAlg.h"
#include "../ElectronSelectorAlg.h"
#include "../TauSelectorAlg.h"
#include "../LargeJetGhostVRJetAssociationAlg.h"
#include "../TruthParentDecoratorAlg.h"
#include "../JetDeepCopyAlg.h"
#include "../TauDecoratorAlg.h"
#include "../TruthParticleInformationAlg.h"

using namespace Easyjet;

DECLARE_COMPONENT(BTaggingDecoratorAlg)
DECLARE_COMPONENT(EventCounterAlg)
DECLARE_COMPONENT(GhostAssocVRJetGetterAlg)
DECLARE_COMPONENT(JetSelectorAlg)
DECLARE_COMPONENT(PhotonSelectorAlg)
DECLARE_COMPONENT(MuonSelectorAlg)
DECLARE_COMPONENT(ElectronSelectorAlg)
DECLARE_COMPONENT(TauSelectorAlg)
DECLARE_COMPONENT(LargeJetGhostVRJetAssociationAlg)
DECLARE_COMPONENT(TruthParentDecoratorAlg)
DECLARE_COMPONENT(JetDeepCopyAlg)
DECLARE_COMPONENT(TauDecoratorAlg)
DECLARE_COMPONENT(TruthParticleInformationAlg)
